package main

import (
	"crypto/tls"
	"flag"
	"io"
	"log"
	"net"
	"strings"
	"time"

	"golang.org/x/crypto/acme"
	"golang.org/x/crypto/acme/autocert"
)

func main() {
	dest := flag.String("dest", "127.0.0.1:80", "Which TCP ip:port to forward the traffic to")
	host := flag.String("host", "", "DNS name to fetch certifcate for")
	bind := flag.String("bind", "0.0.0.0", "If required set bind, not needed in a container")
	cache := flag.String("cachedir", "/acme", "In which folder to store the certs locally")
	flag.Parse()

	if *host == "" {
		log.Fatalln("You have to set `-host`")
	}

	m := &autocert.Manager{
		Cache:      autocert.DirCache(*cache),
		Prompt:     autocert.AcceptTOS,
		HostPolicy: autocert.HostWhitelist(*host),
	}

	config := &tls.Config{
		NextProtos:     []string{acme.ALPNProto},
		GetCertificate: m.GetCertificate,
		MinVersion:     tls.VersionTLS13,
	}

	ln, err := tls.Listen("tcp", *bind+":443", config)
	if err != nil {
		log.Fatalln("Failed to listen:", err)
	}

	defer ln.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		go proxyConn(conn, *dest)
	}
}

// proxyConn, called as goroutine to handle a single connection.
func proxyConn(connection net.Conn, backendAddr string) {
	defer connection.Close()

	d := net.Dialer{Timeout: 5 * time.Second}
	backend, err := d.Dial("tcp", backendAddr)
	if err != nil {
		log.Println("Backend Connection Failure:", err)
		return
	}
	defer backend.Close()

	stop := make(chan bool)

	go fuseStream(backend, connection, stop)
	go fuseStream(connection, backend, stop)

	<-stop
}

// fuseStream, a wrapper around io.Copy.
func fuseStream(from io.WriteCloser, to io.ReadCloser, stop chan bool) {
	defer from.Close()
	defer to.Close()
	_, err := io.Copy(from, to)
	if err != nil && !strings.HasSuffix(err.Error(), "use of closed network connection") {
		log.Println(err)
	}
	stop <- true
}
