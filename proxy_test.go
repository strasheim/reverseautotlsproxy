package main

import (
	"bufio"
	"net"
	"testing"
	"time"
)

func TestProxyConn(t *testing.T) {
	t.Parallel()
	// this test is ugly and noisy - yet it tests both existing functions

	// Create a fake server - like the ratproxy, just without TLS
	fs := fakeserver(t, fakeBackend(t).Addr().String())

	// Creating a fake clients connection
	d := net.Dialer{Timeout: 1 * time.Second}
	fake1, err := d.Dial("tcp", fs.Addr().String())
	if err != nil {
		t.Errorf("Failed to talk to the fake server: %s ", err)
	}
	_, err = fake1.Write([]byte("Unit tests are ...\n"))
	if err != nil {
		t.Errorf("Fake client 1 failed to write data to the fake server: %s ", err)
	}

	spacingTime()
	fake2, err := d.Dial("tcp", fs.Addr().String())
	if err != nil {
		t.Errorf("Failed to talk to the fake server: %s ", err)
	}
	_, err = fake2.Write([]byte("often longer than the code\n"))
	if err != nil {
		t.Errorf("Fake client 2 failed to write data to the fake server: %s ", err)
	}

	echo, err := bufio.NewReader(fake1).ReadString('\n')
	if err != nil {
		t.Errorf("Fake Client 1 should have been able to read it's echo: %s ", err)
	}
	if echo != "Unit tests are ...\n" {
		t.Errorf("The full text wasn't echoed: %s ", echo)
	}

	_, err = bufio.NewReader(fake2).ReadString('\n')
	if err == nil {
		t.Errorf("Fake Client 2 shouldn't have been able to read it's echo: %s ", err)
	}
}

func spacingTime() {
	time.Sleep(200 * time.Millisecond)
}

func fakeserver(t *testing.T, backend string) net.Listener {
	t.Helper()
	// Creating a fakeserver(fs) to accept a connection and pass those on
	fs, errFs := net.Listen("tcp", "127.0.0.1:0")
	if errFs != nil {
		t.Errorf("Failed to create the server listener: %s ", errFs)
	}
	go func() {
		defer fs.Close()
		for {
			server, err := fs.Accept()
			if err != nil {
				t.Errorf("Failed to accept the client connection: %s ", err)
				return
			}
			go proxyConn(server, backend)
		}
	}()
	return fs
}

func fakeBackend(t *testing.T) net.Listener {
	t.Helper()
	be, errBe := net.Listen("tcp", "127.0.0.1:0")
	if errBe != nil {
		t.Errorf("Failed to create the test listener: %s ", errBe)
	}
	go func() {
		defer be.Close()
		backend, err := be.Accept()
		if err != nil {
			t.Errorf("Failed on the accepting connection: %s ", err)
		}
		defer backend.Close()
		buf := make([]byte, 1024)
		size, err := backend.Read(buf)
		if err != nil {
			t.Errorf("Failed to read data from backend socket: %s ", err)
		}
		data := buf[:size]
		_, err = backend.Write(data)
		if err != nil {
			t.Errorf("Failed to echo data back: %s ", err)
		}

		spacingTime()
	}()
	return be
}
