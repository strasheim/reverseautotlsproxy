
### Reverse Auto TLS proxy for a single domain 

* TLS 1.3 only
* Certificate signing via `Lets encrypt`
* Binds to Port 443
* Using the TLS-ALPN-01 challenge


### How to use 

```
Usage of ./ratproxy:
  -bind string
    	If required set bind, not needed in a container (default "0.0.0.0")
  -cachedir string
    	In which folder to store the certs locally (default "/acme")
  -dest string
    	Which TCP ip:port to forward the traffic to (default "127.0.0.1:80")
  -host string
    	DNS name to fetch certifcate for

```

You have to set **-host**, the others are optional, if run as container 
please see the comprehensive notes in the wiki. 

----

More details in the [wiki](https://gitlab.com/strasheim/reverseautotlsproxy/-/wikis/Notes) 
